# Skype shortcuts to Emojis via autokey



This `script.py` generates `*.txt` and `.*.json` config files to be consumed by [autokey](https://github.com/autokey/autokey).

The script doesn't have any dependencies other than os python 2 or 3.

## Install

1. Install [autokey](https://github.com/autokey/autokey), e.g. for ubuntu:

```
sudo apt install autokey-gtk
```

Make sure it runs on startup

2. Run the script to create files in `output` dir

```
python csv_to_autokey_files.py
```

3. Finally, substitute output dir with the ones in home dir. A helper script is:

```
./sub_files.sh
```

## Misc

This [spreadsheet](https://docs.google.com/spreadsheets/d/1n8H6dwi7R-3T4ZSmMw-3VT-xddbT6kHJR7rGAYhRT1o/edit?ouid=106692489912326496581&usp=sheets_home&ths=true) is used to generate `emojis.csv` which is a `emoji:shortcuts` pairs that match emoji `utf-8` codepoints to old skype shortcuts.

You can find the skype shortcuts on the official [Skype website](https://support.skype.com/en/faq/FA12330/what-is-the-full-list-of-emoticons)

A good place to view the represenations of all emojis on different platforms and their codepoints is [unicode.org](http://www.unicode.org/emoji/charts/full-emoji-list.html) and [emojipedia](https://emojipedia.org/people/)

The process of writing this script is in [pisquared blog](http://pi2squared.blogspot.be/2018/03/bringing-back-skype-emojis-to-my.html)

