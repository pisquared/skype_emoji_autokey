import copy
import csv
import json
import os

subs_format = "<pause><ctrl>+<shift>+u{}<pause>"
config_template = {
    "usageCount": 1,
    "omitTrigger": False,
    "prompt": False,
    "description": ":wink:",
    "abbreviation": {
        "wordChars": "[\\w]",
        "abbreviations": [
        ],
        "immediate": True,
        "ignoreCase": False,
        "backspace": True,
        "triggerInside": False
    },
    "hotkey": {
        "hotKey": None,
        "modifiers": []
    },
    "modes": [
        1
    ],
    "showInTrayMenu": False,
    "matchCase": False,
    "filter": {
        "regex": None,
        "isRecursive": False
    },
    "type": "phrase",
    "sendMode": "kb"
}


def main():
    with open("emojis.csv") as f:
        reader = csv.DictReader(f)
        for row in reader:
            name, emojicode = row['name'], row['emoji']
            shorts = row['short1'], row['short2'], row['short3']
            if not (name and emojicode):
                continue
            with open(os.path.join('output', '.{}.json'.format(name)), 'w+') as jsfile, \
                    open(os.path.join('output', '{}.txt'.format(name)), 'w+') as txtfile:
                txtfile.write(subs_format.format(emojicode))
                config = copy.deepcopy(config_template)
                config["description"] = name
                config["abbreviation"]["abbreviations"] += [s for s in shorts if s]
                jsfile.write(json.dumps(config))


if __name__ == '__main__':
    main()
